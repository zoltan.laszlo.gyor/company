package company;

public class Contract {
	private static String employer;
	private static String employee;
	private static int wage;
	
	public Contract(String employer, String employee, int wage) {
		this.employer = employer;
		this.employee = employee;
		this.wage = wage;
		
	}
	
	public static final Contract VADER = new Contract("Emperor", "Vader", 5000);
	
	public static Contract make(String string) {
		String[] list = string.split(",");
		try {
			if(list.length == 3 && !list[0].isEmpty() && !list[1].isEmpty() && isNumber(list[2]) && Integer.parseInt(list[2]) >= 0) {
				return new Contract(list[0], list[1], Integer.parseInt(list[2]));
			}
		} catch (Exception e) {
				return null;
		}
		return null;
	}
	
	public boolean hasEmployer(String name) {
		return name.equals(this.employer);
	}
	
	public String toString() { 
		return "Contract(" + employee + "," + employer + "," + wage + ")";
	}
	
	private static boolean isNumber(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}	
	
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public int getWage() {
		return wage;
	}
	public void setwage(int wage) {
		this.wage = wage;
	}
	
	
}
