package company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Company {
	private String name;
	private LinkedList<Contract> contracts;
	
	public Company(String name, String fileName){
		this.name = name;
		this.contracts = new LinkedList<Contract>();
		try {
			Scanner sc = new Scanner(fileName);
			while(sc.hasNextLine()) {
				Contract example = Contract.make(sc.nextLine());
				if(example != null) {
					contracts.add(example);
				}
			}
		} catch (Exception e) {
			System.out.println("Error with the file");
		}
	}
	
	public LinkedList<String> employeesOf(String employer){
		LinkedList<String> list = new LinkedList<String>();
		for (int i = 0; i < this.contracts.size(); i++) {
			if(this.contracts.get(i).getEmployer().equals(employer)) {
				list.add(contracts.get(i).getEmployee());
			}
		}
		return list;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Contract contract : contracts) {
			sb.append(contract.toString()+"\n");
		}
		return sb.toString();
	}
	
	public LinkedList<String> employees() {
		LinkedList<String> list = new LinkedList<String>();
		for (Contract contract : contracts) {
			if(!list.contains(contract.getEmployee())) {
				list.add(contract.getEmployee());
			}
			if(!list.contains(contract.getEmployer())) {
				list.add(contract.getEmployer());
			}
		}
		return list;
	}
	
	public LinkedList<String> bosses(){
		LinkedList<String> list = new LinkedList<String>();
		for (Contract contract : contracts) {
			if(!list.contains(contract.getEmployer())) {
				list.add(contract.getEmployer());
			}
		}
		return list;
	}
	
	public String bestBoss() {
		Map<String, Integer> listOfBosses = new HashMap();
		String best = null;
		int max = 0;
		for (Contract contract : contracts) {
			if(listOfBosses.containsKey(contract.getEmployer())) {
				int value = listOfBosses.get(contract.getEmployer())+1;
				listOfBosses.put(contract.getEmployer(), value);
			}else {
				listOfBosses.put(contract.getEmployer(), 1);
			}
		}
		for (Entry<String, Integer> entry : listOfBosses.entrySet()) {
			if(entry.getValue() > max) {
				best = entry.getKey();
				max = entry.getValue();
			}
		}
		return best;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedList<Contract> getContracts() {
		return contracts;
	}

	public void setContracts(LinkedList<Contract> contracts) {
		this.contracts = contracts;
	}
}
